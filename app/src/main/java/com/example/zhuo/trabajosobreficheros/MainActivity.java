package com.example.zhuo.trabajosobreficheros;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.zhuo.trabajosobreficheros.utils.RestClient;
import com.jakewharton.picasso.OkHttp3Downloader;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.FileAsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;
import com.squareup.picasso.Picasso;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import okhttp3.OkHttpClient;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private Button btnDownload;
    private ImageView imgMostrar;
    private EditText edtImagenes, edtFrases;
    private TextView txvMostrarFrase;
    private List<String> listaImagenes, listaFrases;
    private int imgSelected, fraseSelected;
    private boolean isImagesDownloaded, isFrasesDownloaded;
    private static final String IMAGENES = "http://alumno.mobi/~alumno/superior/hernandez/imagenes.txt";
    private static final String FRASES = "http://alumno.mobi/~alumno/superior/hernandez/frases.txt";
    private static final String ERRORES = "http://alumno.mobi/~alumno/superior/hernandez/uploads/errores.txt";
    private static final String WEB = "http://alumno.mobi/~alumno/superior/hernandez/upload.php";
    private static final String CODIGO = "UTF-8";
    private static final String NOMBREFICHERO = "errores.txt";
    private CountDownTimer timer;
    private static final int DURACION = 1800000;
    private File archivoErrores;
    private Date currentTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        imgSelected = 0;
        fraseSelected = 0;
        isImagesDownloaded = false;
        isFrasesDownloaded = false;
        btnDownload = findViewById(R.id.btnDescarga);
        btnDownload.setOnClickListener(this);
        imgMostrar = findViewById(R.id.img);
        edtImagenes = findViewById(R.id.edtImg);
        edtFrases = findViewById(R.id.edtFrases);
        txvMostrarFrase = findViewById(R.id.txvMostrarFrase);
        edtImagenes.setText(IMAGENES);
        edtFrases.setText(FRASES);
        listaImagenes = new ArrayList<>();
        listaFrases = new ArrayList<>();
        archivoErrores = new File(getApplicationContext().getFilesDir(), NOMBREFICHERO);
    }

    @Override
    protected void onStart() {
        super.onStart();
        descargaErrores(ERRORES);
    }

    @Override
    public void onClick(View v) {
        if (v == btnDownload){
            descargaImagenes(edtImagenes.getText().toString());
            descargaFrases(edtFrases.getText().toString());
            try
            {
                InputStream archivo = getResources().openRawResource(R.raw.intervalo);
                BufferedReader brin = new BufferedReader(new InputStreamReader(archivo));
                int intervalo = Integer.parseInt(brin.readLine());
                if(timer != null){
                    timer.cancel();
                }
                timer = new CountDownTimer(DURACION, intervalo * 1000) {
                    public void onTick(long millisUntilFinished) {
                        nextImage();
                        nextFrase();
                        //Toast.makeText(getApplicationContext(), "Cambiando imagen y frase...", Toast.LENGTH_LONG).show();
                    }
                    public void onFinish() {
                        Toast.makeText(getApplicationContext(), "Se ha detenido el temporizador", Toast.LENGTH_LONG).show();
                    }
                }.start();

                archivo.close();
            }
            catch (IOException ex)
            {
                Toast.makeText(MainActivity.this, "Error al leer fichero desde recurso raw", Toast.LENGTH_SHORT).show();
            }
            btnDownload.setActivated(false);
        }
    }

    private void descargaImagenes(String url)
    {
        AsyncHttpClient client = new AsyncHttpClient();
        client.get(url, new FileAsyncHttpResponseHandler(/* Context */ this) {
            ProgressDialog pd;
            @Override
            public void onStart() {
                pd = new ProgressDialog(MainActivity.this);
                pd.setTitle("Por favor espere...");
                pd.setMessage("Descarga en progreso");
                pd.setIndeterminate(false);
                pd.setCancelable(false);
                pd.show();
                Toast.makeText(MainActivity.this, "Descargando...", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {
                Toast.makeText(MainActivity.this, "Se ha producido un error al descargar la imagen", Toast.LENGTH_SHORT).show();
                enviarError("Se ha producido un error al descargar la imagen", IMAGENES);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, File file) {
                FileInputStream fis = null;
                try {
                    fis = new FileInputStream(file);
                    BufferedReader in = new BufferedReader(new InputStreamReader(fis));

                    String aLine = null;
                    while ((aLine = in.readLine()) != null) {
                        if(!aLine.trim().isEmpty()) {
                            listaImagenes.add(aLine);
                        }
                    }
                    in.close();
                    fis.close();
                    isImagesDownloaded = true;
                    Toast.makeText(MainActivity.this, "Imágenes cargadas con éxito", Toast.LENGTH_SHORT).show();
                } catch (FileNotFoundException e) {
                    enviarError("El archivo imagenes.txt no se ha encontrado", IMAGENES);
                } catch (IOException e) {
                    enviarError("Ha habido un error al abrir el archivo imagenes.txt", IMAGENES);
                }
            }

            @Override
            public void onFinish() {
                pd.dismiss();
                if(isImagesDownloaded) {
                    loadImage(listaImagenes.get(imgSelected).toString());
                }
            }

        });
    }

    private void descargaFrases(String url)
    {
        AsyncHttpClient client = new AsyncHttpClient();
        client.get(url, new FileAsyncHttpResponseHandler(/* Context */ this) {
            ProgressDialog pd;
            @Override
            public void onStart() {
                pd = new ProgressDialog(MainActivity.this);
                pd.setTitle("Por favor espere..");
                pd.setMessage("Descarga en progreso");
                pd.setIndeterminate(false);
                pd.setCancelable(false);
                pd.show();
                Toast.makeText(MainActivity.this, "Descargando...", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {
                Toast.makeText(MainActivity.this, "Se ha producido un error al descargar la frase", Toast.LENGTH_SHORT).show();
                enviarError("Se ha producido un error al descargar la frase", FRASES);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, File file) {
                FileInputStream fis = null;
                try {
                    fis = new FileInputStream(file);
                    BufferedReader in = new BufferedReader(new InputStreamReader(fis));

                    String aLine = null;
                    while ((aLine = in.readLine()) != null) {
                        if(!aLine.trim().isEmpty()) {
                            listaFrases.add(aLine);
                        }
                    }
                    in.close();
                    fis.close();
                    isFrasesDownloaded = true;
                    Toast.makeText(MainActivity.this, "Frases cargadas con éxito", Toast.LENGTH_SHORT).show();
                } catch (FileNotFoundException e) {
                    enviarError("El archivo frases.txt no se ha encontrado", FRASES);
                } catch (IOException e) {
                    enviarError("Ha habido un error al abrir el archivo frases.txt", FRASES);
                }
            }

            @Override
            public void onFinish() {
                pd.dismiss();
                if(isFrasesDownloaded) {
                    loadFrase(listaFrases.get(fraseSelected).toString());
                }

            }

        });
    }
    private void loadImage(String ruta) {

        OkHttpClient client = new OkHttpClient();
        Picasso picasso = new Picasso.Builder(this).downloader(new OkHttp3Downloader(client)).build();
        picasso.with(this)
                .load(ruta)
                .placeholder(R.drawable.placeholder) // Carga
                .error(R.drawable.error) // Error
                .into(imgMostrar);

    }

    private void loadFrase(String frase) {
        txvMostrarFrase.setText(frase);
    }

    private void nextImage(){
        if(isImagesDownloaded) {
            imgSelected = (imgSelected + 1) % listaImagenes.size();
            loadImage(listaImagenes.get(imgSelected));
        }
    }

    private void nextFrase(){
        if (isFrasesDownloaded) {
            fraseSelected = (fraseSelected + 1) % listaFrases.size();
            loadFrase(listaFrases.get(fraseSelected));
        }
    }

    /*private void previousImage(){
        imgSelected = (imgSelected + listaImagenes.size() - 1) % listaImagenes.size();
        loadImage(listaImagenes.get(imgSelected));
    }*/

    @Override
    protected void onStop() {
        super.onStop();
        timer.cancel();
    }

    private void subida(String fichero) {
        final ProgressDialog progreso = new ProgressDialog(this);
        File myFile;
        Boolean existe = true;
        myFile = new File(fichero);
        RequestParams params = new RequestParams();
        try {
            params.put("fileToUpload", myFile);
        } catch (FileNotFoundException e) {
            existe = false;
            Toast.makeText(getApplicationContext(), "Error en el fichero: " + e.getMessage(), Toast.LENGTH_SHORT).show();
        }
        if (existe)
            RestClient.post(WEB, params, new TextHttpResponseHandler() {
                @Override
                public void onStart() {
                    // called before request is started
                    progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progreso.setMessage("Enviando error . . .");
                    //progreso.setCancelable(false);
                    progreso.setOnCancelListener(new DialogInterface.OnCancelListener() {
                        public void onCancel(DialogInterface dialog) {
                            RestClient.cancelRequests(getApplicationContext(), true);
                        }
                    });
                    progreso.show();
                }

                public void onSuccess(int statusCode, Header[] headers, String response) {
                    // called when response HTTP status is "200 OK"
                    progreso.dismiss();
                    Toast.makeText(getApplicationContext(), response, Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String response, Throwable t) {
                    // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                    progreso.dismiss();
                    Toast.makeText(getApplicationContext(), "ERROR: " + response, Toast.LENGTH_SHORT).show();
                }
            });
    }

    private void descargaErrores(String url)
    {
        archivoErrores.delete();
        AsyncHttpClient client = new AsyncHttpClient();
        client.get(url, new FileAsyncHttpResponseHandler(/* Context */ this) {
            ProgressDialog pd;
            @Override
            public void onStart() {
                pd = new ProgressDialog(MainActivity.this);
                pd.setTitle("Por favor espere...");
                pd.setMessage("Iniciando aplicación");
                pd.setIndeterminate(false);
                pd.setCancelable(false);
                pd.show();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {
                Toast.makeText(MainActivity.this, "Se ha producido un error al descargar errores.txt", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, File file) {
                FileInputStream fis = null;
                try {
                    fis = new FileInputStream(file);
                    BufferedReader in = new BufferedReader(new InputStreamReader(fis));

                    String aLine = null;
                    while ((aLine = in.readLine()) != null) {
                        escribir(archivoErrores, aLine, true, CODIGO);
                    }
                    in.close();
                    fis.close();
                    Toast.makeText(MainActivity.this, "Errores cargados con éxito", Toast.LENGTH_SHORT).show();

                } catch (FileNotFoundException e) {
                    Toast.makeText(MainActivity.this, "El archivo errores.txt no se ha encontrado", Toast.LENGTH_SHORT).show();
                } catch (IOException e) {
                    Toast.makeText(MainActivity.this, "Ha habido un error al abrir el archivo errores.txt", Toast.LENGTH_SHORT).show();
                }
                //Toast.makeText(MainActivity.this, "Archivo errores.txt descargado con éxito", Toast.LENGTH_SHORT).show();
            }
            @Override
            public void onFinish() {
                pd.dismiss();
            }

        });
    }

    private void enviarError(String error, String ruta){
        escribir(archivoErrores, prepararError(error, ruta), true, CODIGO);
        subida(archivoErrores.getPath());
    }

    private String prepararError(String error, String ruta){
        currentTime = new Date();
        return "Ruta: " + ruta + ". Fecha: " + currentTime + ". Error: " + error;

    }

    private boolean escribir(File fichero, String cadena, Boolean anadir, String codigo) {

        FileOutputStream fos = null;
        OutputStreamWriter osw = null;
        BufferedWriter out = null;
        boolean correcto = false;
        try {
            fos = new FileOutputStream(fichero, anadir);
            osw = new OutputStreamWriter(fos, codigo);
            out = new BufferedWriter(osw);
            out.write(cadena);
            out.newLine();
        } catch (IOException e) {
            Log.e("Error de E/S", e.getMessage());
        } finally {
            try {
                if (out != null) {
                    out.close();
                    correcto = true;
                }
            } catch (IOException e) {
                Log.e("Error al cerrar", e.getMessage());
            }
        }
        return correcto;
    }

}
